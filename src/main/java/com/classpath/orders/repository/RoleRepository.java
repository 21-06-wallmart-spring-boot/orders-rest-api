package com.classpath.orders.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.classpath.orders.model.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer>{

}
