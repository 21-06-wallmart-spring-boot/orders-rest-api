package com.classpath.orders.dto;

import java.time.LocalDate;


public interface OrderDto {
	public long getId();
	public String getCustomerName();
	public String getEmail();
	public LocalDate getOrderDate();
	public double getPrice();
}
