package com.classpath.orders.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	@GetMapping
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name="page", required=false, defaultValue="0") int page,
			@RequestParam(name="size", required=false, defaultValue="10") int size,
			@RequestParam(name="order", required=false, defaultValue="asc") String order,
			@RequestParam(name="field", required=false, defaultValue="customerName") String property){
		
		Sort.Direction direction = order.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		
		return this.orderService.fetchAllOrders(page, size, direction, property);
	}
	
	@GetMapping("/price")
	public Map<String, Object> fetchAllOrdersByPriceRange(
			@RequestParam(name="page", required=false, defaultValue="0") int page,
			@RequestParam(name="size", required=false, defaultValue="10") int size,
			@RequestParam(name="order", required=false, defaultValue="asc") String order,
			@RequestParam(name="field", required=false, defaultValue="customerName") String property,
			@RequestParam(name="min", required=false, defaultValue="2000") double min,
			@RequestParam(name="max", required=false, defaultValue="5000") double max
			){
		
		Sort.Direction direction = order.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		
		return this.orderService.findOrdersByPriceRange(page, size, direction, property, min, max);
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable("id") long orderId) {
		return this.orderService.findOrderById(orderId);
	}
	
	@PostMapping
	@ResponseStatus(CREATED)
	public Order saveOrder(@RequestBody @Valid Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long orderId) {
		this.orderService.deleteOrderById(orderId);
	}
}
