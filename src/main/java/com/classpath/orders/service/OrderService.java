package com.classpath.orders.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.classpath.orders.dto.OrderDto;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {
	
	private final OrderRepository orderRepository;
	
	
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Map<String, Object> fetchAllOrders(int page, int size, Sort.Direction direction, String property){
		
		Pageable pageRequest = PageRequest.of(page, size, direction, property);
		Page<OrderDto> pageResponse = this.orderRepository.findBy(pageRequest);
		
		int pageNumber = pageResponse.getNumber();
		int numberOrRecordsPerPage = pageResponse.getNumberOfElements();
		long totalRecords = pageResponse.getTotalElements();
		long totalPages = pageResponse.getTotalPages();
		List<OrderDto> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("page-number", pageNumber);
		response.put("records", numberOrRecordsPerPage);
		response.put("total-records", totalRecords);
		response.put("pages", totalPages);
		response.put("data", data);

		return response;
	}
	
	public Order findOrderById(long orderId) {
		return this.orderRepository.findById(orderId)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}
	
	public Map<String, Object> findOrdersByPriceRange(int page, int size, Sort.Direction direction, String property, double minPrice, double maxPrice) {
		Pageable pageRequest = PageRequest.of(page, size, direction, property);
		Page<OrderDto> pageResponse = this.orderRepository.findByPriceBetween(minPrice, maxPrice, pageRequest);
		
		int pageNumber = pageResponse.getNumber();
		int numberOrRecordsPerPage = pageResponse.getNumberOfElements();
		long totalRecords = pageResponse.getTotalElements();
		long totalPages = pageResponse.getTotalPages();
		List<OrderDto> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("page-number", pageNumber);
		response.put("records", numberOrRecordsPerPage);
		response.put("total-records", totalRecords);
		response.put("pages", totalPages);
		response.put("data", data);

		return response;
	}
}
