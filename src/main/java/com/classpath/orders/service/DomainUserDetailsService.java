package com.classpath.orders.service;

import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.classpath.orders.model.DomainUserDetails;
import com.classpath.orders.model.User;
import com.classpath.orders.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Primary
public class DomainUserDetailsService implements UserDetailsService {
	
	private final UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {
		User user = this.userRepository
							.findByEmail(emailAddress)
							.orElseThrow(() -> new UsernameNotFoundException("invalid email address"));
		//write an adaptor to convert our business domain user to UserDetails object
		return new DomainUserDetails(user);
	}

}
