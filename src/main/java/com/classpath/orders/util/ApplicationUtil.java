package com.classpath.orders.util;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ApplicationUtil implements CommandLineRunner {
	
	private final ApplicationContext applicationContext;
	
	public ApplicationUtil(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void run(String... args) throws Exception {
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		
		//imperative style of programming
		//how to do - write the algorithm to perform the task
		/*
		 * for(String bean: beanNames) { System.out.println("Bean name :: "+ bean); }
		 */
		
		//declarative style of programming
		Arrays.asList(beanNames)
			.stream()
			.filter(bean -> bean.startsWith("user"))
			.forEach(bean -> System.out.println("Bean name :: "+ bean));
	}
}
