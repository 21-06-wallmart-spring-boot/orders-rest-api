package com.classpath.orders.util;

import static java.util.stream.IntStream.range;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import com.classpath.orders.model.LineItem;
import com.classpath.orders.model.Order;
import com.classpath.orders.model.Role;
import com.classpath.orders.model.User;
import com.classpath.orders.repository.OrderRepository;
import com.classpath.orders.repository.RoleRepository;
import com.classpath.orders.repository.UserRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@RequiredArgsConstructor
@Slf4j
@Profile({"dev", "qa"})
//public class BootstrapApplicationData implements ApplicationListener<ApplicationReadyEvent>{
public class BootstrapApplicationData {	
	private final OrderRepository orderRepository;
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;
	
	private final Faker faker = new Faker();
	
	@Value("${app.orders.count}")
	private int records;

	//@Override
	@EventListener
	public void onApplicationEvent(ApplicationReadyEvent event) {
		
		log.info("==========Application is ready for accepting request ===========");
		//Order order = new Order("Ramesh", "Vinay", true, true, 23 ,45);
		range(1,  this.records).forEach(index -> {
			Order order  = Order.builder()
								.customerName(faker.name().firstName())
								.orderDate(faker.date().past(4,  TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.email(faker.internet().emailAddress())
								.build();
			IntStream.range(1, faker.number().numberBetween(2, 5))
					.forEach(val -> {
						LineItem lineItem = LineItem
												.builder()
													.name(faker.commerce().productName())
													.pricePerUnit(faker.number().randomDouble(2, 200, 500))
													.qty(faker.number().numberBetween(2, 4))
													.build();

						//setting the bidirectinoal relationship
						/*
						 * order.getLineItems().add(lineItem); lineItem.setOrder(order);
						 */
						order.addLineItem(lineItem);
					});
			//persist the order
			double totalOrderPrice = order
									.getLineItems()
									.stream()
									.map(lineItem -> lineItem.getPricePerUnit() * lineItem.getQty())
									.reduce((val1, val2) -> val1 + val2).orElse(0d);
				order.setPrice(totalOrderPrice);
				
				this.orderRepository.save(order);
		});
		
		Role userRole = new Role();
		userRole.setName("ROLE_USER");
		this.roleRepository.save(userRole);
		
		Role adminRole = new Role();
		adminRole.setName("ROLE_ADMIN");
		this.roleRepository.save(adminRole);
		
		User kiran = new User();
		kiran.setEmail("kiran@gmail.com");
		kiran.setPassword("welcome");
		kiran.addRole(userRole);
		
		this.userRepository.save(kiran);
		
		User vinay = new User();
		vinay.setEmail("vinay@gmail.com");
		vinay.addRole(adminRole);
		vinay.setPassword("welcome");
		this.userRepository.save(vinay);
		
	}
}
