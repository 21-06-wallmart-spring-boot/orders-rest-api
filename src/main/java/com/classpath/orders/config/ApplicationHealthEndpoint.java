package com.classpath.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Configuration;
import com.classpath.orders.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

//@Configuration
@RequiredArgsConstructor
@Slf4j
public class ApplicationHealthEndpoint implements HealthIndicator{
	
	//private final OrderRepository orderRepository;

	@Override
	public Health health() {
		return Health.status(Status.UP).withDetail("DB-staus",
				  "DB is up").build();
		//long count = this.orderRepository.count();
		/*
		 * if (count > 0 ) { return Health.status(Status.UP).withDetail("DB-staus",
		 * "DB is up").build(); } return
		 * Health.status(Status.DOWN).withDetail("DB-staus", "DB is down").build();
		 */	}

}
