package com.classpath.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

	//private final UserDetailsService userDetailsService;

	// authenticating users for Database authenticated users
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * 
	 * auth .inMemoryAuthentication() .withUser("kiran").password("welcome")
	 * .roles("USER") .and() .withUser("vinay").password("welcome") .roles("ADMIN")
	 * .and() .passwordEncoder(getInstance());
	 * 
	 * auth.userDetailsService(this.userDetailsService)
	 * .passwordEncoder(NoOpPasswordEncoder.getInstance()); }
	 */

	// Authorization rules at the path level
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().disable();
		http.headers().frameOptions().disable();
		http.csrf().disable();
		http.authorizeRequests()
				.antMatchers("/h2-console/**")
					.permitAll()
				.antMatchers(HttpMethod.GET, "/api/orders/**", "/api/orders**")
					.hasAnyRole("Everyone", "super_admins", "admins")
				.antMatchers(HttpMethod.POST, "/api/orders/**")
					.hasAnyRole("admins")
				.antMatchers(HttpMethod.DELETE, "/api/orders/**")
					.hasAnyRole("super_admins", "admins")
				.anyRequest()
                    .fullyAuthenticated()
				.and()
                .oauth2ResourceServer()
                .jwt();

	}
	
	 @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}
