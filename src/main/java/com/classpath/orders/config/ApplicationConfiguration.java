package com.classpath.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {
	
	@ConditionalOnProperty(prefix="app", name="loadUser", havingValue="true")
	@Bean
	public User userBasedOnConditionProperty() {
		return new User();
	}
	
	@ConditionalOnBean(name="userBasedOnConditionProperty")
	@Bean
	public User userBasedOnUserBean() {
		return new User();
	}
	
	@ConditionalOnMissingBean(name="userBasedOnConditionProperty")
	@Bean
	public User userBasedOnMissingUserBean() {
		return new User();
	}

}


class User {
	private long id;
	private String name;
	
	public User() {}
}
